from fastapi import FastAPI

app = FastAPI()

@app.get("/")
async def hello(name: str = "Suvit"):
    return {"Text": f'hello from {name}'}